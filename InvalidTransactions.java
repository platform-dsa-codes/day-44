import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Solution {
    public List<String> invalidTransactions(String[] transactions) {
        List<String> invalid = new ArrayList<>();
        Map<String, List<Transaction>> map = new HashMap<>();

        // Parse transactions and store them in a map
        for (String transaction : transactions) {
            Transaction t = new Transaction(transaction);
            if (!map.containsKey(t.name)) {
                map.put(t.name, new ArrayList<>());
            }
            map.get(t.name).add(t);
        }

        // Check each transaction for validity
        for (String name : map.keySet()) {
            List<Transaction> transList = map.get(name);
            for (int i = 0; i < transList.size(); i++) {
                Transaction ti = transList.get(i);
                if (ti.amount > 1000) {
                    invalid.add(ti.toString());
                    continue;
                }
                for (int j = 0; j < transList.size(); j++) {
                    if (i != j) {
                        Transaction tj = transList.get(j);
                        if (!ti.city.equals(tj.city) && Math.abs(ti.time - tj.time) <= 60) {
                            invalid.add(ti.toString());
                            break;
                        }
                    }
                }
            }
        }
        return invalid;
    }

    class Transaction {
        String name;
        int time;
        int amount;
        String city;

        public Transaction(String transaction) {
            String[] parts = transaction.split(",");
            name = parts[0];
            time = Integer.parseInt(parts[1]);
            amount = Integer.parseInt(parts[2]);
            city = parts[3];
        }

        @Override
        public String toString() {
            return name + "," + time + "," + amount + "," + city;
        }
    }
}
