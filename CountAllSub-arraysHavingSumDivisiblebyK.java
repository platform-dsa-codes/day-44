import java.util.*;

public class Solution {

    public static int subArrayCount(ArrayList<Integer> arr, int k) {
        int count = 0;
        Map<Integer, Integer> remainderMap = new HashMap<>();
        int sum = 0;
        
        for (int i = 0; i < arr.size(); i++) {
            sum += arr.get(i);
            int remainder = ((sum % k) + k) % k; // Ensure remainder is non-negative
            if (remainder == 0)
                count++;
            if (remainderMap.containsKey(remainder))
                count += remainderMap.get(remainder);
            remainderMap.put(remainder, remainderMap.getOrDefault(remainder, 0) + 1);
        }
        
        return count;
    }
    
    public static void main(String[] args) {
        // Sample test case
        ArrayList<Integer> arr = new ArrayList<>(Arrays.asList(5, 0, 2, 3, 1));
        int k = 5;
        System.out.println(subArrayCount(arr, k)); // Output: 6
    }
}
